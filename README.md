READ ME

Requirements for this application to run

1. MYSQL
2. Zend Framework enabled Web Server
3. GD Extension enabled in php

Please ensure the above requirements are met in order for this application to run on you System.

Step 1 DATABASE SETUP

After ensuring the above requirements are met, please setup MySQL Database

1. Create a database newstand database
2. Create a database user for newstand database with the following credentials
	username - newstand
	password - newstandv1.0
3. Run the database script included in this directory to setup your newstand tables

Step 2 APPLICATION SETUP

1. Drop the newstand folder into your webserver document root
2. Run the application via your browser e.g. http://localhost/newstand
3. The table created above has a sample user with the following credentials
	username - olamide
	password - 1234
4. You can also create a new user by simply registering on the platform, please ensure your server setup allows for mail sending to enable verification link to be sent.