
    /*Jssor*/
    (function(h, f, b, i, c, g, j) { /*! Jssor */
        new(function() {});
        var d = {
            Yb: function(a) {
                return -b.cos(a * b.PI) / 2 + .5
            },
            ac: function(a) {
                return a
            },
            wd: function(a) {
                return -a * (a - 2)
            }
        };
        var a = new function() {
            var e = this,
                R = 1,
                fb = 2,
                kb = 3,
                jb = 4,
                ob = 5,
                X, s = 0,
                l = 0,
                p = 0,
                A = 0,
                S = navigator,
                tb = S.appName,
                k = S.userAgent,
                z;

            function Eb() {
                X = X || {
                    jd: "ontouchstart" in h || "createTouch" in f
                };
                return X
            }

            function v(i) {
                if (!s) {
                    s = -1;
                    if (tb == "Microsoft Internet Explorer" && !!h.attachEvent && !!h.ActiveXObject) {
                        var e = k.indexOf("MSIE");
                        s = R;
                        p = n(k.substring(e + 5, k.indexOf(";", e))); /*@cc_on@*/
                        l = f.documentMode || p
                    } else if (tb == "Netscape" && !!h.addEventListener) {
                        var d = k.indexOf("Firefox"),
                            b = k.indexOf("Safari"),
                            g = k.indexOf("Chrome"),
                            c = k.indexOf("AppleWebKit");
                        if (d >= 0) {
                            s = fb;
                            l = n(k.substring(d + 8))
                        } else if (b >= 0) {
                            var j = k.substring(0, b).lastIndexOf("/");
                            s = g >= 0 ? jb : kb;
                            l = n(k.substring(j + 1, b))
                        } else {
                            var a = /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/i.exec(k);
                            if (a) {
                                s = R;
                                l = p = n(a[1])
                            }
                        }
                        if (c >= 0) A = n(k.substring(c + 12))
                    } else {
                        var a = /(opera)(?:.*version|)[ \/]([\w.]+)/i.exec(k);
                        if (a) {
                            s = ob;
                            l = n(a[2])
                        }
                    }
                }
                return i == s
            }

            function q() {
                return v(R)
            }

            function M() {
                return q() && (l < 6 || f.compatMode == "BackCompat")
            }

            function ib() {
                return v(kb)
            }

            function hb() {
                return v(jb)
            }

            function nb() {
                return v(ob)
            }

            function K() {
                return q() && l < 9
            }

            function t(a) {
                if (!z) {
                    m(["transform", "WebkitTransform", "msTransform", "MozTransform", "OTransform"], function(b) {
                        if (a.style[b] != j) {
                            z = b;
                            return c
                        }
                    });
                    z = z || "transform"
                }
                return z
            }

            function sb(a) {
                return {}.toString.call(a)
            }
            var J;

            function Cb() {
                if (!J) {
                    J = {};
                    m(["Boolean", "Number", "String", "Function", "Array", "Date", "RegExp", "Object"], function(a) {
                        J["[object " + a + "]"] = a.toLowerCase()
                    })
                }
                return J
            }

            function m(a, d) {
                if (sb(a) == "[object Array]") {
                    for (var b = 0; b < a.length; b++)
                        if (d(a[b], b, a)) return c
                } else
                    for (var e in a)
                        if (d(a[e], e, a)) return c
            }

            function C(a) {
                return a == i ? String(a) : Cb()[sb(a)] || "object"
            }

            function F(a) {
                try {
                    return C(a) == "object" && !a.nodeType && a != a.window && (!a.constructor || {}.hasOwnProperty.call(a.constructor.prototype, "isPrototypeOf"))
                } catch (b) {}
            }

            function vb(b, a) {
                setTimeout(b, a || 0)
            }

            function H(b, d, c) {
                var a = !b || b == "inherit" ? "" : b;
                m(d, function(c) {
                    var b = c.exec(a);
                    if (b) {
                        var d = a.substr(0, b.index),
                            e = a.substr(b.lastIndex + 1, a.length - (b.lastIndex + 1));
                        a = d + e
                    }
                });
                a = c + (a.indexOf(" ") != 0 ? " " : "") + a;
                return a
            }

            function eb(b, a) {
                if (l < 9) b.style.filter = a
            }
            e.sd = Eb;
            e.rc = q;
            e.pd = ib;
            e.qd = hb;
            e.Fc = K;
            e.Hc = function() {
                return l
            };
            e.Ic = function() {
                v();
                return A
            };
            e.E = vb;

            function V(a) {
                a.constructor === V.caller && a.Dc && a.Dc.apply(a, V.caller.arguments)
            }
            e.Dc = V;
            e.Fb = function(a) {
                if (e.Vd(a)) a = f.getElementById(a);
                return a
            };

            function r(a) {
                return a || h.event
            }
            e.Mc = r;
            e.Eb = function(a) {
                a = r(a);
                return a.target || a.srcElement || f
            };
            e.Nc = function(a) {
                a = r(a);
                return {
                    x: a.pageX || a.clientX || 0,
                    y: a.pageY || a.clientY || 0
                }
            };

            function D(c, d, a) {
                if (a != j) c.style[d] = a;
                else {
                    var b = c.currentStyle || c.style;
                    a = b[d];
                    if (a == "" && h.getComputedStyle) {
                        b = c.ownerDocument.defaultView.getComputedStyle(c, i);
                        b && (a = b.getPropertyValue(d) || b[d])
                    }
                    return a
                }
            }

            function Y(b, c, a, d) {
                if (a != j) {
                    d && (a += "px");
                    D(b, c, a)
                } else return n(D(b, c))
            }

            function o(d, a) {
                var b = a & 2,
                    c = a ? Y : D;
                return function(e, a) {
                    return c(e, d, a, b)
                }
            }

            function Ab(b) {
                if (q() && p < 9) {
                    var a = /opacity=([^)]*)/.exec(b.style.filter || "");
                    return a ? n(a[1]) / 100 : 1
                } else return n(b.style.opacity || "1")
            }

            function Bb(c, a, f) {
                if (q() && p < 9) {
                    var h = c.style.filter || "",
                        i = new RegExp(/[\s]*alpha\([^\)]*\)/g),
                        e = b.round(100 * a),
                        d = "";
                    if (e < 100 || f) d = "alpha(opacity=" + e + ") ";
                    var g = H(h, [i], d);
                    eb(c, g)
                } else c.style.opacity = a == 1 ? "" : b.round(a * 100) / 100
            }
            e.Yd = function(b, c) {
                var a = t(b);
                if (a) b.style[a + "Origin"] = c
            };
            e.Sd = function(a, c) {
                if (q() && p < 9 || p < 10 && M()) a.style.zoom = c == 1 ? "" : c;
                else {
                    var b = t(a);
                    if (b) {
                        var f = "scale(" + c + ")",
                            e = a.style[b],
                            g = new RegExp(/[\s]*scale\(.*?\)/g),
                            d = H(e, [g], f);
                        a.style[b] = d
                    }
                }
            };
            e.ce = function(a) {
                if (!a.style[t(a)] || a.style[t(a)] == "none") a.style[t(a)] = "perspective(2000px)"
            };
            var mb = 0,
                gb = 0;
            e.Ud = function(b, a) {
                return K() ? function() {
                    var h = c,
                        d = M() ? b.document.body : b.document.documentElement;
                    if (d) {
                        var f = d.offsetWidth - mb,
                            e = d.offsetHeight - gb;
                        if (f || e) {
                            mb += f;
                            gb += e
                        } else h = g
                    }
                    h && a()
                } : a
            };
            e.mc = function(b, a) {
                return function(c) {
                    c = r(c);
                    var f = c.type,
                        d = c.relatedTarget || (f == "mouseout" ? c.toElement : c.fromElement);
                    (!d || d !== a && !e.de(a, d)) && b(c)
                }
            };
            e.a = function(a, c, d, b) {
                a = e.Fb(a);
                if (a.addEventListener) {
                    c == "mousewheel" && a.addEventListener("DOMMouseScroll", d, b);
                    a.addEventListener(c, d, b)
                } else if (a.attachEvent) {
                    a.attachEvent("on" + c, d);
                    b && a.setCapture && a.setCapture()
                }
            };
            e.U = function(a, c, d, b) {
                a = e.Fb(a);
                if (a.removeEventListener) {
                    c == "mousewheel" && a.removeEventListener("DOMMouseScroll", d, b);
                    a.removeEventListener(c, d, b)
                } else if (a.detachEvent) {
                    a.detachEvent("on" + c, d);
                    b && a.releaseCapture && a.releaseCapture()
                }
            };
            e.wb = function(a) {
                a = r(a);
                a.preventDefault && a.preventDefault();
                a.cancel = c;
                a.returnValue = g
            };
            e.ae = function(a) {
                a = r(a);
                a.stopPropagation && a.stopPropagation();
                a.cancelBubble = c
            };
            e.Hb = function(d, c) {
                var a = [].slice.call(arguments, 2),
                    b = function() {
                        var b = a.concat([].slice.call(arguments, 0));
                        return c.apply(d, b)
                    };
                return b
            };
            e.W = function(d, c) {
                for (var b = [], a = d.firstChild; a; a = a.nextSibling)(c || a.nodeType == 1) && b.push(a);
                return b
            };

            function rb(a, c, e, b) {
                b = b || "u";
                for (a = a ? a.firstChild : i; a; a = a.nextSibling)
                    if (a.nodeType == 1) {
                        if (Q(a, b) == c) return a;
                        if (!e) {
                            var d = rb(a, c, e, b);
                            if (d) return d
                        }
                    }
            }
            e.r = rb;

            function lb(a, c, d) {
                for (a = a ? a.firstChild : i; a; a = a.nextSibling)
                    if (a.nodeType == 1) {
                        if (a.tagName == c) return a;
                        if (!d) {
                            var b = lb(a, c, d);
                            if (b) return b
                        }
                    }
            }
            e.Hd = lb;

            function B() {
                var e = arguments,
                    d, c, b, a, g = 1 & e[0],
                    f = 1 + g;
                d = e[f - 1] || {};
                for (; f < e.length; f++)
                    if (c = e[f])
                        for (b in c) {
                            a = c[b];
                            if (a !== j) {
                                a = c[b];
                                d[b] = g && F(d[b]) ? B(g, {}, a) : a
                            }
                        }
                    return d
            }
            e.K = B;
            e.Od = function(a) {
                return C(a) == "function"
            };
            e.Vd = function(a) {
                return C(a) == "string"
            };
            e.Qd = function(a) {
                return !isNaN(n(a)) && isFinite(a)
            };
            e.i = m;

            function N(a) {
                return f.createElement(a)
            }
            e.Z = function() {
                return N("DIV")
            };
            e.Xb = function() {};

            function T(b, c, a) {
                if (a == j) return b.getAttribute(c);
                b.setAttribute(c, a)
            }

            function Q(a, b) {
                return T(a, b) || T(a, "data-" + b)
            }
            e.cb = T;
            e.n = Q;

            function w(b, a) {
                if (a == j) return b.className;
                b.className = a
            }
            e.fc = w;
            e.B = function(a) {
                e.C(a, "none")
            };
            e.ab = function(a, b) {
                e.C(a, b ? "none" : "")
            };
            e.ec = function(b, a) {
                b.removeAttribute(a)
            };
            e.Yc = function() {
                return q() && l < 10
            };
            e.bd = function(d, c) {
                if (c) d.style.clip = "rect(" + b.round(c.e) + "px " + b.round(c.k) + "px " + b.round(c.j) + "px " + b.round(c.f) + "px)";
                else {
                    var g = d.style.cssText,
                        f = [new RegExp(/[\s]*clip: rect\(.*?\)[;]?/i), new RegExp(/[\s]*cliptop: .*?[;]?/i), new RegExp(/[\s]*clipright: .*?[;]?/i), new RegExp(/[\s]*clipbottom: .*?[;]?/i), new RegExp(/[\s]*clipleft: .*?[;]?/i)],
                        e = H(g, f, "");
                    a.Gb(d, e)
                }
            };
            e.u = function() {
                return +new Date
            };
            e.G = function(b, a) {
                b.appendChild(a)
            };
            e.zb = function(b, a, c) {
                (c || a.parentNode).insertBefore(b, a)
            };
            e.Ec = function(a, b) {
                (b || a.parentNode).removeChild(a)
            };
            e.dd = function(a, b) {
                m(a, function(a) {
                    e.Ec(a, b)
                })
            };
            e.hc = function(a) {
                e.dd(e.W(a, c), a)
            };
            var n = parseFloat;
            e.ad = n;
            e.de = function(b, a) {
                var c = f.body;
                while (a && b !== a && c !== a) try {
                    a = a.parentNode
                } catch (d) {
                    return g
                }
                return b === a
            };

            function U(d, c, b) {
                var a = d.cloneNode(!c);
                !b && e.ec(a, "id");
                return a
            }
            e.L = U;
            e.N = function(f, g) {
                var a = new Image;

                function b(f, c) {
                    e.U(a, "load", b);
                    e.U(a, "abort", d);
                    e.U(a, "error", d);
                    g && g(a, c)
                }

                function d(a) {
                    b(a, c)
                }
                if (nb() && l < 11.6 || !f) b(!f);
                else {
                    e.a(a, "load", b);
                    e.a(a, "abort", d);
                    e.a(a, "error", d);
                    a.src = f
                }
            };
            e.Zc = function(d, a, f) {
                var c = d.length + 1;

                function b(b) {
                    c--;
                    if (a && b && b.src == a.src) a = b;
                    !c && f && f(a)
                }
                m(d, function(a) {
                    e.N(a.src, b)
                });
                b()
            };
            e.lb = D;
            e.hb = o("overflow");
            e.M = o("top", 2);
            e.P = o("left", 2);
            e.m = o("width", 2);
            e.p = o("height", 2);
            e.od = o("marginLeft", 2);
            e.kd = o("marginTop", 2);
            e.A = o("position");
            e.C = o("display");
            e.o = o("zIndex", 1);
            e.Sb = function(b, a, c) {
                if (a != j) Bb(b, a, c);
                else return Ab(b)
            };
            e.Gb = function(a, b) {
                if (b != j) a.style.cssText = b;
                else return a.style.cssText
            };
            var P = {
                    t: e.Sb,
                    e: e.M,
                    f: e.P,
                    yb: e.m,
                    Bb: e.p,
                    T: e.A,
                    oe: e.C,
                    mb: e.o
                },
                u;

            function I() {
                if (!u) u = B({
                    me: e.kd,
                    ne: e.od,
                    c: e.bd
                }, P);
                return u
            }
            e.ed = I;
            e.D = function(c, b) {
                var a = I();
                m(b, function(d, b) {
                    a[b] && a[b](c, d)
                })
            };
            new(function() {});
            e.gd = function(l, g, t, r, u, w, j) {
                var c = g;
                if (l) {
                    c = {};
                    for (var f in g) {
                        var x = w[f] || 1,
                            s = u[f] || [0, 1],
                            e = (t - s[0]) / s[1];
                        e = b.min(b.max(e, 0), 1);
                        e = e * x;
                        var p = b.floor(e);
                        if (e != p) e -= p;
                        var v = r[f] || r.sb || d.Yb,
                            q = v(e),
                            h, y = l[f];
                        g[f];
                        var m = g[f];
                        if (a.Qd(m)) h = y + m * q;
                        else {
                            h = a.K({
                                S: {}
                            }, l[f]);
                            a.i(m.S, function(c, b) {
                                var a = c * q;
                                h.S[b] = a;
                                h[b] += a
                            })
                        }
                        c[f] = h
                    }
                    if (g.l || g.s);
                }
                if (g.c && j.ob) {
                    var k = c.c.S,
                        o = (k.e || 0) + (k.j || 0),
                        n = (k.f || 0) + (k.k || 0);
                    c.f = (c.f || 0) + n;
                    c.e = (c.e || 0) + o;
                    c.c.f -= n;
                    c.c.k -= n;
                    c.c.e -= o;
                    c.c.j -= o
                }
                if (c.c && a.Yc() && !c.c.e && !c.c.f && c.c.k == j.eb && c.c.j == j.kb) c.c = i;
                return c
            }
        };

        function l() {
            var b = this,
                d = [];

            function i(a, b) {
                d.push({
                    Rb: a,
                    Qb: b
                })
            }

            function g(b, c) {
                a.i(d, function(a, e) {
                    a.Rb == b && a.Qb === c && d.splice(e, 1)
                })
            }
            b.Y = b.addEventListener = i;
            b.removeEventListener = g;
            b.g = function(b) {
                var c = [].slice.call(arguments, 1);
                a.i(d, function(a) {
                    a.Rb == b && a.Qb.apply(h, c)
                })
            }
        }

        function k(n, z, j, R, P, K) {
            n = n || 0;
            var e = this,
                r, o, p, x, A = 0,
                I, J, H, C, E = 0,
                l = 0,
                u = 0,
                D, m = n,
                k, f, q, y = [],
                B;

            function M(a) {
                k += a;
                f += a;
                m += a;
                l += a;
                u += a;
                E = a
            }

            function Q(a, b) {
                var c = a - k + n * b;
                M(c);
                return f
            }

            function w(h, n) {
                var d = h;
                if (q && (d >= f || d <= k)) d = ((d - k) % q + q) % q + k;
                if (!D || x || n || l != d) {
                    var g = b.min(d, f);
                    g = b.max(g, k);
                    if (!D || x || n || g != u) {
                        if (K) {
                            var i = (g - m) / (z || 1);
                            if (j.Lc) i = 1 - i;
                            var o = a.gd(P, K, i, I, H, J, j);
                            a.i(o, function(b, a) {
                                B[a] && B[a](R, b)
                            })
                        }
                        e.Ab(u - m, g - m)
                    }
                    u = g;
                    a.i(y, function(b, c) {
                        var a = h < l ? y[y.length - c - 1] : b;
                        a.v(h - E, n)
                    });
                    var r = l,
                        p = h;
                    l = d;
                    D = c;
                    e.ib(r, p)
                }
            }

            function F(a, c, d) {
                c && a.pc(f, 1);
                !d && (f = b.max(f, a.R() + E));
                y.push(a)
            }
            var s = h.requestAnimationFrame || h.webkitRequestAnimationFrame || h.mozRequestAnimationFrame || h.msRequestAnimationFrame;
            if (a.pd() && a.Hc() < 7) s = i;
            s = s || function(b) {
                a.E(b, j.Ac)
            };

            function L() {
                if (r) {
                    var d = a.u(),
                        e = b.min(d - A, j.zc),
                        c = l + e * p;
                    A = d;
                    if (c * p >= o * p) c = o;
                    w(c);
                    if (!x && c * p >= o * p) N(C);
                    else s(L)
                }
            }

            function v(d, g, h) {
                if (!r) {
                    r = c;
                    x = h;
                    C = g;
                    d = b.max(d, k);
                    d = b.min(d, f);
                    o = d;
                    p = o < l ? -1 : 1;
                    e.Cc();
                    A = a.u();
                    s(L)
                }
            }

            function N(a) {
                if (r) {
                    x = r = C = g;
                    e.Bc();
                    a && a()
                }
            }
            e.xc = function(a, b, c) {
                v(a ? l + a : f, b, c)
            };
            e.wc = v;
            e.H = N;
            e.vd = function(a) {
                v(a)
            };
            e.z = function() {
                return l
            };
            e.cc = function() {
                return o
            };
            e.O = function() {
                return u
            };
            e.v = w;
            e.tc = function() {
                w(k, c)
            };
            e.ob = function(a) {
                w(l + a)
            };
            e.yc = function() {
                return r
            };
            e.zd = function(a) {
                q = a
            };
            e.pc = Q;
            e.vc = M;
            e.Db = function(a) {
                F(a, 0)
            };
            e.Cb = function(a) {
                F(a, 1)
            };
            e.R = function() {
                return f
            };
            e.ib = e.Cc = e.Bc = e.Ab = a.Xb;
            e.Jb = a.u();
            j = a.K({
                Ac: 16,
                zc: 50
            }, j);
            q = j.gc;
            B = a.K({}, a.ed(), j.Wb);
            k = m = n;
            f = n + z;
            J = j.qb || {};
            H = j.nb || {};
            I = a.K({
                sb: a.Od(j.q) && j.q || d.Yb
            }, j.q)
        }
        new(function() {});
        var e = function(q, ec) {
            var n = this;

            function Cc() {
                var a = this;
                k.call(a, -1e8, 2e8);
                a.Wc = function() {
                    var c = a.O(),
                        d = b.floor(c),
                        f = t(d),
                        e = c - b.floor(c);
                    return {
                        J: f,
                        Xc: d,
                        T: e
                    }
                };
                a.ib = function(d, a) {
                    var f = b.floor(a);
                    if (f != a && a > d) f++;
                    Sb(f, c);
                    n.g(e.Rc, t(a), t(d), a, d)
                }
            }

            function Bc() {
                var b = this;
                k.call(b, 0, 0, {
                    gc: s
                });
                a.i(C, function(a) {
                    D & 1 && a.zd(s);
                    b.Cb(a);
                    a.vc(ib / ac)
                })
            }

            function Ac() {
                var a = this,
                    b = Rb.F;
                k.call(a, -1, 2, {
                    q: d.ac,
                    Wb: {
                        T: Yb
                    },
                    gc: s
                }, b, {
                    T: 1
                }, {
                    T: -2
                });
                a.Ib = b
            }

            function pc(m, l) {
                var a = this,
                    d, f, h, j, b;
                k.call(a, -1e8, 2e8, {
                    zc: 100
                });
                a.Cc = function() {
                    R = c;
                    V = i;
                    n.g(e.Tc, t(y.z()), y.z())
                };
                a.Bc = function() {
                    R = g;
                    j = g;
                    var a = y.Wc();
                    n.g(e.cd, t(y.z()), y.z());
                    !a.T && Ec(a.Xc, r)
                };
                a.ib = function(g, e) {
                    var a;
                    if (j) a = b;
                    else {
                        a = f;
                        if (h) {
                            var c = e / h;
                            a = p.ic(c) * (f - d) + d
                        }
                    }
                    y.v(a)
                };
                a.db = function(b, e, c, g) {
                    d = b;
                    f = e;
                    h = c;
                    y.v(b);
                    a.v(0);
                    a.wc(c, g)
                };
                a.Qc = function(d) {
                    j = c;
                    b = d;
                    a.xc(d, i, c)
                };
                a.Uc = function(a) {
                    b = a
                };
                y = new Cc;
                y.Db(m);
                y.Db(l)
            }

            function qc() {
                var c = this,
                    b = Wb();
                a.o(b, 0);
                a.lb(b, "pointerEvents", "none");
                c.F = b;
                c.jb = function() {
                    a.B(b);
                    a.hc(b)
                }
            }

            function zc(o, h) {
                var d = this,
                    q, x, I, y, j, B = [],
                    G, u, U, H, P, F, f, w, m;
                k.call(d, -v, v + 1, {});

                function E(a) {
                    x && x.bb();
                    q && q.bb();
                    R(o, a);
                    F = c;
                    q = new N.I(o, N, 1);
                    x = new N.I(o, N);
                    x.tc();
                    q.tc()
                }

                function X() {
                    q.Jb < N.Jb && E()
                }

                function J(o, r, m) {
                    if (!H) {
                        H = c;
                        if (j && m) {
                            var f = m.width,
                                b = m.height,
                                l = f,
                                k = b;
                            if (f && b && p.V) {
                                if (p.V & 3 && (!(p.V & 4) || f > L || b > K)) {
                                    var i = g,
                                        q = L / K * b / f;
                                    if (p.V & 1) i = q > 1;
                                    else if (p.V & 2) i = q < 1;
                                    l = i ? f * K / b : L;
                                    k = i ? K : b * L / f
                                }
                                a.m(j, l);
                                a.p(j, k);
                                a.M(j, (K - k) / 2);
                                a.P(j, (L - l) / 2)
                            }
                            a.A(j, "absolute");
                            n.g(e.Nd, h)
                        }
                    }
                    a.B(r);
                    o && o(d)
                }

                function W(b, c, e, f) {
                    if (f == V && r == h && S)
                        if (!Dc) {
                            var a = t(b);
                            A.Sc(a, h, c, d, e);
                            c.Md();
                            bb.pc(a, 1);
                            bb.v(a);
                            z.db(b, b, 0)
                        }
                }

                function ab(b) {
                    if (b == V && r == h) {
                        if (!f) {
                            var a = i;
                            if (A)
                                if (A.J == h) a = A.fd();
                                else A.jb();
                            X();
                            f = new wc(o, h, a, d.Ld(), d.Pd());
                            f.lc(m)
                        }!f.yc() && f.Mb()
                    }
                }

                function Q(e, c, g) {
                    if (e == h) {
                        if (e != c) C[c] && C[c].Pc();
                        else !g && f && f.Fd();
                        m && m.uc();
                        var j = V = a.u();
                        d.N(a.Hb(i, ab, j))
                    } else {
                        var l = b.abs(h - e),
                            k = v + p.Ed - 1;
                        (!P || l <= k) && d.N()
                    }
                }

                function db() {
                    if (r == h && f) {
                        f.H();
                        m && m.Kd();
                        m && m.Jd();
                        f.bc()
                    }
                }

                function gb() {
                    r == h && f && f.H()
                }

                function Z(a) {
                    !O && n.g(e.Id, h, a)
                }

                function M() {
                    m = w.pInstance;
                    f && f.lc(m)
                }
                d.N = function(d, b) {
                    b = b || y;
                    if (B.length && !H) {
                        a.ab(b);
                        if (!U) {
                            U = c;
                            n.g(e.Rd, h);
                            a.i(B, function(b) {
                                if (!a.cb(b, "src")) {
                                    b.src = a.n(b, "src2");
                                    a.C(b, b["display-origin"])
                                }
                            })
                        }
                        a.Zc(B, j, a.Hb(i, J, d, b))
                    } else J(d, b)
                };
                d.be = function() {
                    if (A) {
                        var b = A.Vc(s);
                        if (b) {
                            var e = V = a.u(),
                                c = h + Vb,
                                d = C[t(c)];
                            return d.N(a.Hb(i, W, c, d, b, e), y)
                        }
                    }
                    cb(r + p.jc * Vb)
                };
                d.tb = function() {
                    Q(h, h, c)
                };
                d.Pc = function() {
                    m && m.Kd();
                    m && m.Jd();
                    d.kc();
                    f && f.Wd();
                    f = i;
                    E()
                };
                d.Md = function() {
                    a.B(o)
                };
                d.kc = function() {
                    a.ab(o)
                };
                d.Td = function() {
                    m && m.uc()
                };

                function R(b, e, d) {
                    if (a.cb(b, "jssor-slider")) return;
                    d = d || 0;
                    if (!F) {
                        if (b.tagName == "IMG") {
                            B.push(b);
                            if (!a.cb(b, "src")) {
                                P = c;
                                b["display-origin"] = a.C(b);
                                a.B(b)
                            }
                        }
                        a.Fc() && a.o(b, (a.o(b) || 0) + 1);
                        if (p.Oc && a.Ic())(!Y || a.Ic() < 534 || !fb && !a.qd()) && a.ce(b)
                    }
                    var f = a.W(b);
                    a.i(f, function(f) {
                        var i = f.tagName,
                            k = a.n(f, "u");
                        if (k == "player" && !w) {
                            w = f;
                            if (w.pInstance) M();
                            else a.a(w, "dataavailable", M)
                        }
                        if (k == "caption") {
                            if (!a.rc() && !e) {
                                var h = a.L(f, g, c);
                                a.zb(h, f, b);
                                a.Ec(f, b);
                                f = h;
                                e = c
                            }
                        } else if (!F && !d && !j) {
                            if (i == "A") {
                                if (a.n(f, "u") == "image") j = a.Hd(f, "IMG");
                                else j = a.r(f, "image", c);
                                if (j) {
                                    G = f;
                                    a.D(G, T);
                                    u = a.L(G, c);
                                    a.C(u, "block");
                                    a.D(u, T);
                                    a.Sb(u, 0);
                                    a.lb(u, "backgroundColor", "#000")
                                }
                            } else if (i == "IMG" && a.n(f, "u") == "image") j = f;
                            if (j) {
                                j.border = 0;
                                a.D(j, T)
                            }
                        }
                        R(f, e, d + 1)
                    })
                }
                d.Ab = function(c, b) {
                    var a = v - b;
                    Yb(I, a)
                };
                d.Ld = function() {
                    return q
                };
                d.Pd = function() {
                    return x
                };
                d.J = h;
                l.call(d);
                var D = a.r(o, "thumb", c);
                if (D) {
                    a.L(D);
                    a.ec(D, "id");
                    a.B(D)
                }
                a.ab(o);
                y = a.L(eb);
                a.o(y, 1e3);
                a.a(o, "click", Z);
                E(c);
                d.hd = j;
                d.Jc = u;
                d.Ib = I = o;
                a.G(I, y);
                n.Y(203, Q);
                n.Y(28, gb);
                n.Y(24, db)
            }

            function wc(E, i, p, u, t) {
                var b = this,
                    l = 0,
                    w = 0,
                    m, h, d, f, j, q, v, s, o = C[i];
                k.call(b, 0, 0);

                function x() {
                    a.hc(P);
                    cc && j && o.Jc && a.G(P, o.Jc);
                    a.ab(P, !j && o.hd)
                }

                function y() {
                    if (q) {
                        q = g;
                        n.g(e.Xd, i, d, l, h, d, f);
                        b.v(h)
                    }
                    b.Mb()
                }

                function z(a) {
                    s = a;
                    b.H();
                    b.Mb()
                }
                b.Mb = function() {
                    var a = b.O();
                    if (!B && !R && !s && r == i) {
                        if (!a) {
                            if (m && !j) {
                                j = c;
                                b.bc(c);
                                n.g(e.Zd, i, l, w, m, f)
                            }
                            x()
                        }
                        var g, p = e.Gc;
                        if (a != f)
                            if (a == d) g = f;
                            else if (a == h) g = d;
                        else if (!a) g = h;
                        else if (a > d) {
                            q = c;
                            g = d;
                            p = e.rd
                        } else g = b.cc();
                        n.g(p, i, a, l, h, d, f);
                        var k = S && (!H || I);
                        if (a == f)(d != f && !(H & 12) || k) && o.be();
                        else(k || a != d) && b.wc(g, y)
                    }
                };
                b.Fd = function() {
                    d == f && d == b.O() && b.v(h)
                };
                b.Wd = function() {
                    A && A.J == i && A.jb();
                    var a = b.O();
                    a < f && n.g(e.Gc, i, -a - 1, l, h, d, f)
                };
                b.bc = function(b) {
                    p && a.hb(kb, b && p.Pb.je ? "" : "hidden")
                };
                b.Ab = function(b, a) {
                    if (j && a >= m) {
                        j = g;
                        x();
                        o.kc();
                        A.jb();
                        n.g(e.ud, i, l, w, m, f)
                    }
                    n.g(e.td, i, a, l, h, d, f)
                };
                b.lc = function(a) {
                    if (a && !v) {
                        v = a;
                        a.Y($JssorPlayer$.xd, z)
                    }
                };
                p && b.Cb(p);
                m = b.R();
                b.R();
                b.Cb(u);
                h = u.R();
                d = h + (a.ad(a.n(E, "idle")) || oc);
                t.vc(d);
                b.Db(t);
                f = b.R()
            }

            function Yb(g, f) {
                var e = w > 0 ? w : jb,
                    c = Ab * f * (e & 1),
                    d = Bb * f * (e >> 1 & 1);
                c = b.round(c);
                d = b.round(d);
                a.P(g, c);
                a.M(g, d)
            }

            function Mb() {
                rb = R;
                Ib = z.cc();
                F = y.z()
            }

            function fc() {
                Mb();
                if (B || !I && H & 12) {
                    z.H();
                    n.g(e.id)
                }
            }

            function dc(e) {
                if (!B && (I || !(H & 12)) && !z.yc()) {
                    var c = y.z(),
                        a = b.ceil(F);
                    if (e && b.abs(G) >= p.Kb) {
                        a = b.ceil(c);
                        a += hb
                    }
                    if (!(D & 1)) a = b.min(s - v, b.max(a, 0));
                    var d = b.abs(a - c);
                    d = 1 - b.pow(1 - d, 5);
                    if (!O && rb) z.vd(Ib);
                    else if (c == a) {
                        ub.Td();
                        ub.tb()
                    } else z.db(c, a, d * Tb)
                }
            }

            function Gb(b) {
                !a.n(a.Eb(b), "nodrag") && a.wb(b)
            }

            function tc(a) {
                Xb(a, 1)
            }

            function Xb(b, d) {
                b = a.Mc(b);
                var k = a.Eb(b);
                if (!M && !a.n(k, "nodrag") && uc() && (!d || b.touches.length == 1)) {
                    B = c;
                    zb = g;
                    V = i;
                    a.a(f, d ? "touchmove" : "mousemove", Cb);
                    a.u();
                    O = 0;
                    fc();
                    if (!rb) w = 0;
                    if (d) {
                        var j = b.touches[0];
                        vb = j.clientX;
                        wb = j.clientY
                    } else {
                        var h = a.Nc(b);
                        vb = h.x;
                        wb = h.y
                    }
                    G = 0;
                    db = 0;
                    hb = 0;
                    n.g(e.nd, t(F), F, b)
                }
            }

            function Cb(e) {
                if (B) {
                    e = a.Mc(e);
                    var f;
                    if (e.type != "mousemove") {
                        var l = e.touches[0];
                        f = {
                            x: l.clientX,
                            y: l.clientY
                        }
                    } else f = a.Nc(e);
                    if (f) {
                        var j = f.x - vb,
                            k = f.y - wb;
                        if (b.floor(F) != F) w = w || jb & M;
                        if ((j || k) && !w) {
                            if (M == 3)
                                if (b.abs(k) > b.abs(j)) w = 2;
                                else w = 1;
                            else w = M;
                            if (Y && w == 1 && b.abs(k) - b.abs(j) > 3) zb = c
                        }
                        if (w) {
                            var d = k,
                                i = Bb;
                            if (w == 1) {
                                d = j;
                                i = Ab
                            }
                            if (!(D & 1)) {
                                if (d > 0) {
                                    var g = i * r,
                                        h = d - g;
                                    if (h > 0) d = g + b.sqrt(h) * 5
                                }
                                if (d < 0) {
                                    var g = i * (s - v - r),
                                        h = -d - g;
                                    if (h > 0) d = -g - b.sqrt(h) * 5
                                }
                            }
                            if (G - db < -2) hb = 0;
                            else if (G - db > 2) hb = -1;
                            db = G;
                            G = d;
                            tb = F - G / i / (ab || 1);
                            if (G && w && !zb) {
                                a.wb(e);
                                if (!R) z.Qc(tb);
                                else z.Uc(tb)
                            }
                        }
                    }
                }
            }

            function pb() {
                rc();
                if (B) {
                    B = g;
                    a.u();
                    a.U(f, "mousemove", Cb);
                    a.U(f, "touchmove", Cb);
                    O = G;
                    z.H();
                    var b = y.z();
                    n.g(e.md, t(b), b, t(F), F);
                    H & 12 && Mb();
                    dc(c)
                }
            }

            function jc(c) {
                if (O) {
                    a.ae(c);
                    var b = a.Eb(c);
                    while (b && u !== b) {
                        b.tagName == "A" && a.wb(c);
                        try {
                            b = b.parentNode
                        } catch (d) {
                            break
                        }
                    }
                }
            }

            function nc(a) {
                C[r];
                r = t(a);
                ub = C[r];
                Sb(a);
                return r
            }

            function Ec(a, b) {
                w = 0;
                nc(a);
                n.g(e.ld, t(a), b)
            }

            function Sb(b, c) {
                xb = b;
                a.i(Q, function(a) {
                    a.Vb(t(b), b, c)
                })
            }

            function uc() {
                var b = e.oc || 0,
                    a = ob;
                if (Y) a & 1 && (a &= 1);
                e.oc |= a;
                return M = a & ~b
            }

            function rc() {
                if (M) {
                    e.oc &= ~ob;
                    M = 0
                }
            }

            function Wb() {
                var b = a.Z();
                a.D(b, T);
                a.A(b, "absolute");
                return b
            }

            function t(a) {
                return (a % s + s) % s
            }

            function kc(a, c) {
                if (c)
                    if (!D) {
                        a = b.min(b.max(a + xb, 0), s - v);
                        c = g
                    } else if (D & 2) {
                    a = t(a + xb);
                    c = g
                }
                cb(a, p.gb, c)
            }

            function yb() {
                a.i(Q, function(a) {
                    a.Nb(a.fb.le <= I)
                })
            }

            function hc() {
                if (!I) {
                    I = 1;
                    yb();
                    if (!B) {
                        H & 12 && dc();
                        H & 3 && C[r].tb()
                    }
                }
            }

            function gc() {
                if (I) {
                    I = 0;
                    yb();
                    B || !(H & 12) || fc()
                }
            }

            function ic() {
                T = {
                    yb: L,
                    Bb: K,
                    e: 0,
                    f: 0
                };
                a.i(W, function(b) {
                    a.D(b, T);
                    a.A(b, "absolute");
                    a.hb(b, "hidden");
                    a.B(b)
                });
                a.D(eb, T)
            }

            function nb(b, a) {
                cb(b, a, c)
            }

            function cb(f, e, k) {
                if (Ob && (!B || p.qc)) {
                    R = c;
                    B = g;
                    z.H();
                    if (e == j) e = Tb;
                    var d = Db.O(),
                        a = f;
                    if (k) {
                        a = d + f;
                        if (f > 0) a = b.ceil(a);
                        else a = b.floor(a)
                    }
                    if (D & 2) a = t(a);
                    if (!(D & 1)) a = b.max(0, b.min(a, s - v));
                    var i = (a - d) % s;
                    a = d + i;
                    var h = d == a ? 0 : e * b.abs(i);
                    h = b.min(h, e * v * 1.5);
                    z.db(d, a, h || 1)
                }
            }
            n.xc = function() {
                if (!S) {
                    S = c;
                    C[r] && C[r].tb()
                }
            };

            function Z() {
                return a.m(x || q)
            }

            function lb() {
                return a.p(x || q)
            }
            n.eb = Z;
            n.kb = lb;

            function Eb(c, d) {
                if (c == j) return a.m(q);
                if (!x) {
                    var b = a.Z(f);
                    a.fc(b, a.fc(q));
                    a.Gb(b, a.Gb(q));
                    a.C(b, "block");
                    a.A(b, "relative");
                    a.M(b, 0);
                    a.P(b, 0);
                    a.hb(b, "visible");
                    x = a.Z(f);
                    a.A(x, "absolute");
                    a.M(x, 0);
                    a.P(x, 0);
                    a.m(x, a.m(q));
                    a.p(x, a.p(q));
                    a.Yd(x, "0 0");
                    a.G(x, b);
                    var k = a.W(q);
                    a.G(q, x);
                    a.lb(q, "backgroundImage", "");
                    var i = {
                        navigator: X && X.X == g,
                        arrowleft: E && E.X == g,
                        arrowright: E && E.X == g,
                        thumbnavigator: J && J.X == g,
                        thumbwrapper: J && J.X == g
                    };
                    a.i(k, function(c) {
                        a.G(i[a.n(c, "u")] ? q : b, c)
                    })
                }
                ab = c / (d ? a.p : a.m)(x);
                a.Sd(x, ab);
                var h = d ? ab * Z() : c,
                    e = d ? c : ab * lb();
                a.m(q, h);
                a.p(q, e);
                a.i(Q, function(a) {
                    a.Tb(h, e)
                })
            }
            n.Ad = Eb;
            l.call(n);
            n.F = q = a.Fb(q);
            var p = a.K({
                V: 0,
                Ed: 1,
                Lb: 0,
                Ob: g,
                nc: 1,
                Oc: c,
                qc: c,
                jc: 1,
                Ub: 3e3,
                xb: 1,
                gb: 500,
                ic: d.wd,
                Kb: 20,
                ub: 0,
                Q: 1,
                sc: 0,
                Kc: 1,
                pb: 1,
                rb: 1
            }, ec);
            if (p.Dd != j) p.Ub = p.Dd;
            if (p.dc != j) p.Q = p.dc;
            var jb = p.pb & 3,
                Vb = (p.pb & 4) / -4 || 1,
                gb = p.ke,
                N = a.K({
                    I: o
                }, p.ge),
                X = p.fe,
                E = p.ee,
                J = p.ie,
                U = !p.Kc,
                x, u = a.r(q, "slides", U),
                eb = a.r(q, "loading", U) || a.Z(f),
                Hb = a.r(q, "navigator", U),
                bc = a.r(q, "arrowleft", U),
                Zb = a.r(q, "arrowright", U),
                Fb = a.r(q, "thumbnavigator", U),
                mc = a.m(u),
                lc = a.p(u),
                T, W = [],
                vc = a.W(u);
            a.i(vc, function(b) {
                if (b.tagName == "DIV" && !a.n(b, "u")) W.push(b);
                else a.Fc() && a.o(b, (a.o(b) || 0) + 1)
            });
            var r = -1,
                xb, ub, s = W.length,
                L = p.Zb || mc,
                K = p.Cd || lc,
                Ub = p.ub,
                Ab = L + Ub,
                Bb = K + Ub,
                ac = jb & 1 ? Ab : Bb,
                v = b.min(p.Q, s),
                kb, w, M, zb, Q = [],
                Nb, Pb, Lb, cc, Dc, S, H = p.xb,
                oc = p.Ub,
                Tb = p.gb,
                sb, fb, ib, Ob = v < s,
                D = Ob ? p.nc : 0,
                ob, O, I = 1,
                R, B, V, vb = 0,
                wb = 0,
                G, db, hb, Db, y, bb, z, Rb = new qc,
                ab;
            S = p.Ob;
            n.fb = ec;
            ic();
            a.cb(q, "jssor-slider", c);
            a.o(u, a.o(u) || 0);
            a.A(u, "absolute");
            kb = a.L(u, c);
            a.zb(kb, u);
            if (gb) {
                cc = gb.he;
                sb = gb.I;
                fb = v == 1 && s > 1 && sb && (!a.rc() || a.Hc() >= 8)
            }
            ib = fb || v >= s || !(D & 1) ? 0 : p.sc;
            ob = (v > 1 || ib ? jb : -1) & p.rb;
            var Qb = u,
                C = [],
                A, P, Y = a.sd().jd,
                F, rb, Ib, tb;
            bb = new Ac;
            if (fb) A = new sb(Rb, L, K, gb, Y);
            a.G(kb, bb.Ib);
            a.hb(u, "hidden");
            P = Wb();
            a.lb(P, "backgroundColor", "#000");
            a.Sb(P, 0);
            a.zb(P, Qb.firstChild, Qb);
            for (var qb = 0; qb < W.length; qb++) {
                var xc = W[qb],
                    yc = new zc(xc, qb);
                C.push(yc)
            }
            a.B(eb);
            Db = new Bc;
            z = new pc(Db, bb);
            if (ob) {
                a.a(u, "mousedown", Xb);
                a.a(u, "touchstart", tc);
                a.a(u, "dragstart", Gb);
                a.a(u, "selectstart", Gb);
                a.a(f, "mouseup", pb);
                a.a(f, "touchend", pb);
                a.a(f, "touchcancel", pb);
                a.a(h, "blur", pb)
            }
            H &= Y ? 10 : 5;
            if (Hb && X) {
                Nb = new X.I(Hb, X, Z(), lb());
                Q.push(Nb)
            }
            if (E && bc && Zb) {
                E.nc = D;
                E.Q = v;
                Pb = new E.I(bc, Zb, E, Z(), lb());
                Q.push(Pb)
            }
            if (Fb && J) {
                J.Lb = p.Lb;
                Lb = new J.I(Fb, J);
                Q.push(Lb)
            }
            a.i(Q, function(a) {
                a.vb(s, C, eb);
                a.Y(m.Bd, kc)
            });
            Eb(Z());
            a.a(u, "click", jc);
            a.a(q, "mouseout", a.mc(hc, q));
            a.a(q, "mouseover", a.mc(gc, q));
            yb();
            p.yd && a.a(f, "keydown", function(a) {
                if (a.keyCode == 37) nb(-1);
                else a.keyCode == 39 && nb(1)
            });
            var mb = p.Lb;
            if (!(D & 1)) mb = b.max(0, b.min(mb, s - v));
            z.db(mb, mb, 0)
        };
        e.Id = 21;
        e.nd = 22;
        e.md = 23;
        e.Tc = 24;
        e.cd = 25;
        e.Rd = 26;
        e.Nd = 27;
        e.id = 28;
        e.Rc = 202;
        e.ld = 203;
        e.Zd = 206;
        e.ud = 207;
        e.td = 208;
        e.Gc = 209;
        e.rd = 210;
        e.Xd = 211;
        var m = {
            Bd: 1
        };

        function o() {
            k.call(this, 0, 0);
            this.bb = a.Xb
        }
        jssor_slider1_starter = function(i) {
            var j = {
                    Ob: c,
                    jc: 1,
                    Ub: 0,
                    xb: 4,
                    yd: c,
                    ic: d.ac,
                    gb: 1600,
                    Kb: 20,
                    Zb: 140,
                    ub: 0,
                    Q: 7,
                    sc: 0,
                    Kc: 1,
                    pb: 1,
                    rb: 1
                },
                g = new e(i, j);

            function f() {
                var c = g.F.parentNode.clientWidth;
                if (c) g.Ad(b.min(c, 980));
                else a.E(f, 30)
            }
            f();
            a.a(h, "load", f);
            a.a(h, "resize", a.Ud(h, f));
            a.a(h, "orientationchange", f)
        }
    })(window, document, Math, null, true, false)