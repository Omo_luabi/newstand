$(document).ready(function(){
    reloadPost();
    var options = {
        beforeSubmit:  beforeSubmit,  // pre-submit callback 
        success:       afterSuccess,  // post-submit callback 
        dataType: 'json',
        resetForm: false        // reset the form after successful submit 
    }; 
    
    $('#MyUploadForm').submit(function() {
        event.preventDefault();
        $(this).ajaxSubmit(options);  			
        return false; 
    });
});

function reloadPost(){
	//show please wait on that page here
	mainPageWait(1);
	dataString = {
		action:"read_post"
	};
    makeApiCall(dataString);
}

function mainPageWait(power){
	if(power === 0){
		//$(".loading").css("display", "none");
		$(".loading").fadeOut(); 
	}else{
		$(".loading").fadeIn();
	}
}

function pleaseWait(type, div){
    if(type === 0){
        $("#"+div).removeClass("alert-danger");
        $("#"+div).text("");
        document.getElementById(div).style.display = "none";
    }else{
        $("#"+div).addClass("alert-danger");
        $("#"+div).text("Please Wait...");
        document.getElementById(div).style.display = "block";
    }
}

function show(what){
    if(what === "logi"){
        document.getElementById('logi').style.display = "block";
        document.getElementById('reg').style.display = "none";
    }else{
        document.getElementById('logi').style.display = "none";
        document.getElementById('reg').style.display = "block";
    }
}

function back(form){
    document.getElementById('verify').style.display = "none";
    showModal('auth');
    show(form);
}

function showModal(type){//Create, rss, img, list,auth
    document.getElementById("all_alert").style.display = "none";
    if(type === "create"){
        $('#modal-title').text("Create a new Post");
        document.getElementById('new_post').style.display = "block";
        document.getElementById('rss').style.display = "none";
        document.getElementById('login').style.display = "none";
    }else if(type === "rss"){
        $('#modal-title').text("Subscribe for feed");
        document.getElementById('new_post').style.display = "none";
        document.getElementById('rss').style.display = "block";
        document.getElementById('login').style.display = "none";
    }else if(type === "auth"){
        $('#modal-title').text("Login");
        document.getElementById('new_post').style.display = "none";
        document.getElementById('rss').style.display = "none";
        document.getElementById('login').style.display = "block";
    }else if(type === "verify"){
        $('#modal-title').text("Create New Password");
        document.getElementById('new_post').style.display = "none";
        document.getElementById('rss').style.display = "none";
        document.getElementById('login').style.display = "none";
        document.getElementById('verify').style.display = "block";
    }
    $('#showmodal').click();
}

function makeApiCall(dataString){
    //console.log(dataString);
    $.ajax({
       type: 'POST',
            url: url,
            data: dataString,
            async: true,
            jsonpCallback: 'jsonCallback',
            dataType: 'json',
            success: function(json) {
            console.log(json.data);
               if(json.data.status === 1){
                   if(dataString.action === "read_post"){
                       if(typeof(Storage) !== "undefined") {
                            localStorage["data"] = JSON.stringify(json.data);
                            showPost("list");
                        } else {
                            showPost("list", json.data);
                        }
                        
                   }else if(dataString.action === "register"){
                       set_alert_timed("all_alert", "success", json.data.status_detail);
                        $("#reg_email").val('');
                        $("#reg_username").val('');
                   }else if(dataString.action === "verify"){
                       //set_alert_timed("all_alert", "success", json.data.status_detail);
                       show_message("all_alert","success", json.data.status_detail);
                        $("#createpassword").val('');
                        $("#verifypassword").val('');
                        document.getElementById('verify').style.display = "none";
                        sureGuy();
                   }else if(dataString.action === "auth"){
                        set_alert_timed("all_alert", "success", json.data.status_detail);
                        $("#logi_username").val('');
                        $("#logi_password").val('');
                        sureGuy();
                        location.reload();
                        $('#closemodal').click();
                   }else if(dataString.action === "logout"){
                       $("li.users_item").css("display", "none");
                       $(".login_item").css("display", "block");
                   }else if(dataString.action === "post_operations"){
                        if(dataString.type === "delete"){
                            $(".post_"+ dataString.post_id).fadeOut(); 
                        }else if(dataString.type === "readfull"){
                                //populate the readfull space
                                $('#full_header h4').html(json.data.title);
                                $('#full_body').html(json.data.result);
                                $('#img').html("<img src='img/"+json.data.image+"' style='max-width:100%'/>");
                        }
                    }
                    mainPageWait(0);
               }else{
                    if(dataString.action === "submit_post" || dataString.action === "submit_job_post"){
                         set_alert_timed("all_alert", "danger", json.data.status_detail);
                     }else{
                         set_alert_timed("all_alert", "danger", json.data.status_detail);
                         console.log("Error occured "+json.data.status_detail);
                     }
               }
            },
            error: function(e) {
               console.log(e);
               pleaseWait(0, "all_alert");
            },
            complete: function(){
                console.log("done");
            }
    });
       
}

function ii(){
    $('#newscontent').val(CKEDITOR.instances.text_editor.getData());
    document.getElementById("submit-btn").click();
}

function sureGuy(){
    //For this authenticated person, show create button and show logout button, hide login
    $("li.users_item").css("display", "block");
    $(".login_item").css("display", "none");
    //reloadPost();
}

function logout(){
     post_array = {
        action:"logout"
    };
    makeApiCall(post_array);
}

function login(){
    p1 = $("#logi_username").val();
    p2 = $("#logi_password").val();
    if(p1 !== undefined && p2 !== undefined){
        post_array = {
            action:"auth",
            password:$("#logi_password").val(),
            username:$("#logi_username").val()
        };
        //insert_post();
        pleaseWait(1, "all_alert");
        makeApiCall(post_array);
    }else{
       set_alert_timed("all_alert", "danger", "Incorrect Login Credentials, please try again."); 
    }
}

function verify(){
    p1 = $("#createpassword").val();
    p2 = $("#verifypassword").val();
    if(p1 != undefined && p1 === p2){
        post_array = {
            action:"verify",
            allow:$("#allow").val(),
            trace:$("#trace").val(),
            password:p1
        };
        //insert_post();
        pleaseWait(1, "all_alert");
        makeApiCall(post_array);
    }else{
       set_alert_timed("all_alert", "danger", "You have not entered your password appropriately, please try again."); 
    }
}

function register(){
    username = $("#reg_username").val();
    email = $("#reg_email").val();
    var ind1 = email.indexOf('@');
    var ind2 = email.indexOf('.');
    if(ind2 != -1 && ind1 != -1 && ind2 >ind1 && username != "" && username != undefined){
        post_array = {
            action:"register",
            username:username,
            useremail:email
        };
        //insert_post();
        pleaseWait(1, "all_alert");
        makeApiCall(post_array);
    }else{
       set_alert_timed("all_alert", "danger", "Please check that you have entered the details correctly."); 
    }
}

function showPost(type, data){
    $('#posts').html("");
    if(data === undefined){
        data = JSON.parse(localStorage.data);
    }
    if(type === "list"){
        for (var i=0; i < data.count; i++) {
            post_array = data.result[i];
            show_posts(post_array);
        }
        document.getElementById("p_list").style.display ="none";
        document.getElementById("p_img").style.display ="block";
    }else{
        for (var i=0; i < data.count; i++) {
            post_array = data.result[i];
            show_posts_box(post_array);
        }
        endit = '<div style="clear:both"></div>';
        document.getElementById("p_img").style.display ="none";
        document.getElementById("p_list").style.display ="block";
        $('#posts').append(endit);
    }
    
}

function show_posts_box(post_array, who){
    img = post_array['newsimage'];
    post_id = post_array['newsid'];
    newstitle = post_array['newstitle'];
    html =  '<div onClick="post_operations('+post_id+',\'readfull\')" class="col-sm-4 col-xs-6" style="text-align: center; cursor: pointer; margin-top:10px; margin-bottom:10px;"><img src="img/'+img+'" style="border:1px solid #ddd; max-width: 100%; border-radius:4px;" alt="News Image"><div>'+newstitle+'</div></div>';
          
    $('#posts').append(html);
}

function show_posts(post_array, who){
    try{
        rd = ((",", post_array['readcount']+"padding").match(/,/g) || []).length;
        cm = ((",", post_array['commentcount']+"padding").match(/,/g) || []).length;
    }catch(Exception){
        rd = '';
        cm = '';
    }
    username = post_array['username'];
    useremail = post_array['useremail'];
    newstitle = post_array['newstitle'];
    post_id = post_array['newsid'];
    preview = (post_array['newscontent']).replace(/(<([^>]+)>)/ig,"");
    if(preview.length > 200){
        preview = preview.substring(0,201) + "...";
    }
    close = '';
	remove = '';
    otherDetails = '';
    if(who === "me" || userid === post_array['userid']){
        close = '<span aria-hidden="true" style="float:right;" onClick=\'post_operations('+post_id+',"delete")\'><i class="icon-trash"></i></span>';//&times;
		remove = "<span class='icon-trash option_item' onClick='post_operations("+post_id+",\"delete\")'> Remove Post </span>";
    }
    op = "<div id='op' class='options'> <span class='icon-user option_item' onClick='post_operations("+post_id+",\"like\")'> Read "+rd+"</span> <span class='icon-comment-alt option_item' onClick='post_operations("+post_id+",\"comment\")'> Comment "+cm+"</span> "+remove+" </div>"+
"<div style='clear:both;'></div>";
    
    html = "<div id='post' class='post_"+post_id+"'><div class='post'>"+
    "<span class='post-name'><img src='img/"+post_array['newsimage']+"' style='' class='img-thumbnail img-post' alt='News Image'> <a onClick='post_operations("+post_id+",\"readfull\")'>"+newstitle+"</a></span>"+close+"<hr style='margin-top:10px;'/>"+
    "<div id='says'><div>"+preview+"</div>"+
    "<div>"+otherDetails+"</div></div><div style='float:right; font-size:smaller; color:#003B4F;'><i> -Post by : "+username+" ("+useremail+")</i></div><br/>"+
    "<div id='time' class='time icon-time'> "+timeSince(post_array['newstimestamp'])+"</div>"+ op +
    "</div></div>";
    if(who === 'me'){
        $('#posts').prepend(html);
    }else{
        $('#posts').append(html);
    }  
}

function goHome(){
	$("#posts").css("display", "block");
	$("#full_message").fadeOut(); 
}

function post_operations(post_id,type){
    //Show please wait on normal page before proceeding with this
	mainPageWait(1);
	if(type === "readfull"){
            $("#full_message").css("display", "block");
            $("#posts").fadeOut(); 
	}
    dataString = {
        action:"post_operations",
        post_id:post_id,
        type:type
    };
    makeApiCall(dataString);
}

function timeSince(date) {
    var seconds = Math.floor((new Date() - new Date(date)) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}

function submit_post(type){//dataString.type
    msg = $("#editor1").val();
    name = user_name;
    if(msg === ""){
        return;
    }
    post_array = {
        action:"submit_post",
        newsid:0,
        userid:user_id,
        newstitle:id,
        newscontent:name,
        newsimage:type
    };
    //insert_post();
    makeApiCall(post_array);
    $("#post_form").val('');
}

function set_alert_timed(div, type, msg){
    show_message(div,type, msg);
    setTimeout(function () {show_message(div, "reset")}, 10000);
}

function show_message(div, type, msg){
    if(type === "reset"){
        $("#"+div).removeClass("alert-danger");
        $("#"+div).removeClass("alert-success");
        document.getElementById(div).style.display = "none";
        return;
    }
    if(type === 'success'){
        $("#"+div).removeClass("alert-danger");
    }else { $("#"+div).removeClass("alert-success"); }
        $("#"+div).addClass("alert-"+type);
        $("#"+div).html(msg);
        document.getElementById(div).style.display = "block";
}

/***************** IMAGE UPLOAD ******************************/

function imgwaiting(type){
    if(type === 0){
        pleaseWait(0, "all_alert");
    }
    pleaseWait(1, "all_alert");
}

$("#imageInput").change(function(){
    //readURL(this);
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img-preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    imgwaiting(0);
}

function afterSuccess(json){ // try to submit the real post now
	if(json.data.status === 1){
		set_alert_timed("all_alert", "success", json.data.status_detail);
		console.log("upload data : "+json);
		$('#posts').html('');
                $('#imageInput').val('');
                $('#newstitle').val('');
		reloadPost();
	}else{
		set_alert_timed("all_alert", "danger", json.data.status_detail);
    }
	// show_posts(post_array);
}
//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
    imgwaiting(1);
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
            if( !$('#imageInput').val()) //check empty input filed
            {
                    imgwaiting(0);
                    show_message("all_alert", "danger", "Are you kidding me? Select an Image");
                    return false;
            }

            var fsize = $('#imageInput')[0].files[0].size; //get file size
            var ftype = $('#imageInput')[0].files[0].type; // get file type

            switch(ftype)
            {
                    case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                            break;
                    default:
                            imgwaiting(0);
                            show_message("all_alert", "danger", "Unsupported file type!");
                            return false;
            }

            if(fsize>1048576) 
            {
                    imgwaiting(0);
                    show_message("all_alert", "danger", "<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
                    return false;
            }
            imgwaiting(1);
    }
    else
    {
            imgwaiting(0);
            show_message("all_alert", "danger", "Please upgrade your browser, because your current browser lacks some new features we need!");
            return false;
    }
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Bytes';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    imgwaiting(0);
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

/*******************IMAGE UPLOAD ****************************/