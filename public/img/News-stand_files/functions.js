$(document).ready(function(){
    dataString = {
                action:"read_post"
            };
    makeApiCall(dataString);
});

function pleaseWait(type, div){
    if(type === 0){
        $("#"+div).removeClass("alert-danger");
        $("#"+div).text("");
        document.getElementById(div).style.display = "none";
    }else{
        $("#"+div).addClass("alert-danger");
        $("#"+div).text("Please Wait...");
        document.getElementById(div).style.display = "block";
    }
}

function show(what){
    if(what === "logi"){
        document.getElementById('logi').style.display = "block";
        document.getElementById('reg').style.display = "none";
    }else{
        document.getElementById('logi').style.display = "none";
        document.getElementById('reg').style.display = "block";
    }
}

function back(form){
    document.getElementById('verify').style.display = "none";
    showModal('auth');
    show(form);
}

function showModal(type){//Create, rss, img, list,auth
    document.getElementById("all_alert").style.display = "none";
    if(type === "create"){
        $('#modal-title').text("Create a new Post");
        document.getElementById('new_post').style.display = "block";
        document.getElementById('rss').style.display = "none";
        document.getElementById('login').style.display = "none";
    }else if(type === "rss"){
        $('#modal-title').text("Subscribe for feed");
        document.getElementById('new_post').style.display = "none";
        document.getElementById('rss').style.display = "block";
        document.getElementById('login').style.display = "none";
    }else if(type === "auth"){
        $('#modal-title').text("Login");
        document.getElementById('new_post').style.display = "none";
        document.getElementById('rss').style.display = "none";
        document.getElementById('login').style.display = "block";
    }else if(type === "verify"){
        $('#modal-title').text("Create New Password");
        document.getElementById('new_post').style.display = "none";
        document.getElementById('rss').style.display = "none";
        document.getElementById('login').style.display = "none";
        document.getElementById('verify').style.display = "block";
    }
    $('#showmodal').click();
}

function makeApiCall(dataString){
    //console.log(dataString);
    $.ajax({
       type: 'POST',
            url: url,
            data: dataString,
            async: true,
            jsonpCallback: 'jsonCallback',
            dataType: 'json',
            success: function(json) {
            console.log(json.data);
               if(json.data.status === 1){
                   if(dataString.action === "read_post"){      
                        for (var i=0; i < json.data.count; i++) {
                            post_array = json.data.result[i];
                            show_posts(post_array);
                        }
                   }else if(dataString.action === "register"){
                       set_alert_timed("all_alert", "success", json.data.status_detail);
                        $("#reg_email").val('');
                        $("#reg_username").val('');
                   }else if(dataString.action === "verify"){
                       //set_alert_timed("all_alert", "success", json.data.status_detail);
                       show_message("all_alert","success", json.data.status_detail);
                        $("#createpassword").val('');
                        $("#verifypassword").val('');
                        document.getElementById('verify').style.display = "none";
                        sureGuy();
                   }else if(dataString.action === "auth"){
                        set_alert_timed("all_alert", "success", json.data.status_detail);
                        $("#logi_username").val('');
                        $("#logi_password").val('');
                        sureGuy();
                        $('#closemodal').click();
                   }else if(dataString.action === "logout"){
                       $("li.users_item").css("display", "none");
                       $(".login_item").css("display", "block");
                   }
               }else{
                    if(dataString.action === "submit_post" || dataString.action === "submit_job_post"){
                         set_alert_timed("all_alert", "danger", json.data.status_detail);
                     }else{
                         set_alert_timed("all_alert", "danger", json.data.status_detail);
                         console.log("Error occured "+json.data.status_detail);
                     }
               }
            },
            error: function(e) {
               console.log(e);
               pleaseWait(0, "all_alert");
            },
            complete: function(){
                console.log("done");
            }
    });
       
}

function sureGuy(){
    //For this authenticated person, show create button and show logout button, hide login
    $("li.users_item").css("display", "block");
    $(".login_item").css("display", "none");
}

function logout(){
     post_array = {
        action:"logout"
    };
    makeApiCall(post_array);
}

function login(){
    p1 = $("#logi_username").val();
    p2 = $("#logi_password").val();
    if(p1 !== undefined && p2 !== undefined){
        post_array = {
            action:"auth",
            password:$("#logi_password").val(),
            username:$("#logi_username").val()
        };
        //insert_post();
        pleaseWait(1, "all_alert");
        makeApiCall(post_array);
    }else{
       set_alert_timed("all_alert", "danger", "Incorrect Login Credentials, please try again."); 
    }
}

function verify(){
    p1 = $("#createpassword").val();
    p2 = $("#verifypassword").val();
    if(p1 != undefined && p1 === p2){
        post_array = {
            action:"verify",
            allow:$("#allow").val(),
            trace:$("#trace").val(),
            password:p1
        };
        //insert_post();
        pleaseWait(1, "all_alert");
        makeApiCall(post_array);
    }else{
       set_alert_timed("all_alert", "danger", "You have not entered your password appropriately, please try again."); 
    }
}

function register(){
    username = $("#reg_username").val();
    email = $("#reg_email").val();
    var ind1 = email.indexOf('@');
    var ind2 = email.indexOf('.');
    if(ind2 != -1 && ind1 != -1 && ind2 >ind1 && username != "" && username != undefined){
        post_array = {
            action:"register",
            username:username,
            useremail:email
        };
        //insert_post();
        pleaseWait(1, "all_alert");
        makeApiCall(post_array);
    }else{
       set_alert_timed("all_alert", "danger", "Please check that you have entered the details correctly."); 
    }
}

function show_posts(post_array, who){
    try{
        rd = ((",", post_array['readcount']+"padding").match(/,/g) || []).length;
        cm = ((",", post_array['commentcount']+"padding").match(/,/g) || []).length;
    }catch(Exception){
        rd = '';
        cm = '';
    }
    username = post_array['username'];
    useremail = post_array['useremail'];
    newstitle = post_array['newstitle'];
    post_id = post_array['newsid'];
    close = '';
    otherDetails = '';
    if(who === "me" || user_id === post_array['userid']){
        close = '<span aria-hidden="true" style="float:right;" onClick=\'post_operations('+post_id+',"delete")\'><i class="icon-trash"></i></span>';//&times;
    }
    op = "<div id='op' class='options'> <span class='icon-user option_item' onClick='post_operations("+post_id+",\"like\")'> Read "+rd+"</span> <span class='icon-comment-alt option_item' onClick='post_operations("+post_id+",\"comment\")'> Comment "+cm+"</span> <span class='icon-trash option_item' onClick='post_operations("+post_id+",\"delete\")'> Remove Post </span> </div>"+
"<div style='clear:both;'></div>";
    
    html = "<div id='post' class='post_"+post_id+"'><div class='post'>"+
    "<span class='post-name'><img src='img/"+post_array['newsimage']+"' style='' class='img-thumbnail img-post' alt='Profile Image'> <a onClick='post_operations("+post_id+",\"readfull\")'>"+newstitle+"</a></span>"+close+"<hr style='margin-top:10px;'/>"+
    "<div id='says'><div>"+post_array['newscontent']+"</div>"+
    "<div>"+otherDetails+"</div></div><div style='float:right; font-size:smaller; color:#003B4F;'><i> -Post by : "+username+" ("+useremail+")</i></div><br/>"+
    "<div id='time' class='time icon-time'> "+timeSince(post_array['newstimestamp'])+"</div>"+ op +
    "</div></div>";
    if(who === 'me'){
        $('#posts').prepend(html);
    }else{
        $('#posts').append(html);
    }  
}

function timeSince(date) {
    var seconds = Math.floor((new Date() - new Date(date)) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}

function submit_post(type){//dataString.type
    msg = $("#editor1").val();
    id = user_id;
    name = user_name;
    if(msg === ""){
        return;
    }
    post_array = {
        action:"submit_post",
        post_id:0,
        post_user_id:id,
        user_name:name,
        post_type:type,
        post_content:msg,
        post_to_id:$("#post_to_id").val(),
        post_name:name,
        post_time:"now",
        user_image:my_img
    };
    //insert_post();
    makeApiCall(post_array);
    $("#post_form").val('');
}

function set_alert_timed(div, type, msg){
    show_message(div,type, msg);
    setTimeout(function () {show_message(div, "reset")}, 10000);
}

function show_message(div, type, msg){
    if(type === "reset"){
        $("#"+div).removeClass("alert-danger");
        $("#"+div).removeClass("alert-success");
        document.getElementById(div).style.display = "none";
        return;
    }
    if(type === 'success'){
        $("#"+div).removeClass("alert-danger");
    }else { $("#"+div).removeClass("alert-success"); }
        $("#"+div).addClass("alert-"+type);
        $("#"+div).text(msg);
        document.getElementById(div).style.display = "block";
}