<?php

class IndexController extends Zend_Controller_Action
{
    //Included try catch in the init function below
    //enable mod_rewrite in apache
    public function init()
    {
        /* Initialize action controller here */
		//$user->is_logged_in() ? $user->get('first_name') : 'Guest'
        try{
            $user_details = new Zend_Session_Namespace('user_details');
            $this->submitAction();
            if(isset($user_details->userid)){
                $this->view->userid = $user_details->userid;
                $this->view->username = $user_details->username;
                $this->view->useremail= $user_details->useremail;
            }else{
                $this->view->userid = 0;
                $this->view->username = "Public";
            }
        }Catch(Exception $er){
            $this->view->userid = 0;
            $this->view->username = "Public";
        }
        
    }

    public function indexAction()
    {
        
    }
    
    public function submitAction(){
        if( filter_input(INPUT_GET, 'action') == 'logout'){
            $user_details = new Zend_Session_Namespace('user_details');
            if($user_details->isLocked()){
                $user_details->unlock();
            }
            Zend_Session::namespaceUnset('user_details');
            //$urlOptions = array('controller'=>'Auth', 'action'=>'index');
        }
    }


}

