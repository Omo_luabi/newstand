<?php
class ApiController extends Zend_Controller_Action
{
//6234221933
    public function init()
    {
        //init for my API Class
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout('authlayout');
    }

    public function indexAction()
    {
//        var_dump($this->register($_REQUEST['useremail'], $_REQUEST['username']));
//        var_dump($this->verify($_REQUEST['trace'], $_REQUEST['allow'], $_REQUEST['password']));
//        $this->authenticate("omoluabi", "678");
    }

    public function registerAction()
    {
        // action body
    }

    public function getAction()
    {
        // action body
        echo "No get";
    }
    
    function plainSend($subject, $message, $to){
//        mail($to, $subject, $message);
        
        $check = md5(sha1("lock this safe|".$subject . $message . $to));
        $subject = urlencode($subject);
        $message = urlencode($message);
        $to = urlencode($to);
        
        $local = str_replace("localhost", "localhost/newstand/public","http://".$_SERVER['HTTP_HOST'] );
        return $local = $local . "/sendMail.php?subject=$subject&message=$message&to=$to&check=$check";
        $sendmail = file_get_contents($local);
        return $sendmail;
    }

    public function postAction()
    {
        $action = $this->_request->getPost('action');
        
        try{
            $user_details = new Zend_Session_Namespace('user_details');
            $user_id = 0;
        }catch(Exception $er){

        }
        
        if(isset($user_details->userid)){
            $this->userid = $user_id = $user_details->userid;
        }
        $result = '';
        switch ($action) {
            case 'auth':
                $result = $this->authenticate($this->_request->getPost('username'), $this->_request->getPost('password'));
            break;
            case 'verify':
                $result = $this->verify($this->_request->getPost('trace'), $this->_request->getPost('allow'));
            break;
            case 'register':
                $result = $this->register($this->_request->getPost('useremail'), $this->_request->getPost('username'));
            break;
            case 'logout':
                $result = $this->logout();
            break;
            case 'upload_news':
                $result = $this->upload_news();
            break;
            case 'submit_post':
                $result = $this->submit_post("image");
            break;
            case 'read_post':
                $result = $this->read_post();
            break;
			case 'post_operations':
                $result = $this->post_operations();
            break;
            case 'read_comment':
                $result = $this->read_comment();
            break;
            default:
                $result = "No Action Specified";
            break;
        }
        //echo Zend_Json::encode($this->_todo);
        echo Zend_Json::encode($result);
    }

    public function putAction()
    {
        // action body
    }

    public function deleteAction()
    {
        // action body
    }
    
    function read_post(){
        $posts = new Application_Model_DbTable_Posts();
        $post_array = $posts->read_recent_posts();
        $count = count($post_array);
        if($count > 0){
            return array("data"=>array("status"=>1, "count"=>$count, "status_detail"=>"Success", "result"=>$post_array));
        }else{
            return array("data"=>array("status"=>0, "status_detail"=>"Failed", "result"=>""));
        }
    }
    
    public function read_comment($user_id,$post_id, $type){
        
    }
    
    function submit_post($newsimage){
        $userid = $this->userid;
        $posts = new Application_Model_DbTable_Posts();
        extract($_POST);
        ///mysql_real_escape_string($newscontent)
        $post_array = array("userid"=>$userid, "newstitle"=>$newstitle, "newscontent"=>$newscontent, "newsimage"=>$newsimage);
        $id = $posts->create_post($post_array);
        if($id){
            return array("data"=>array("status"=>1, "status_detail"=>"Successfully Submitted.", "result"=>$_POST));
        }else{
            return array("data"=>array("status"=>0, "status_detail"=>"Error Submitting.", "result"=>$_POST));
        }
    }
	
	function post_operations(){
		$posts = new Application_Model_DbTable_Posts();
		extract($_POST);
		if($type === "delete"){
			$result = $posts->delete_post($post_id, $this->userid);
			if($result > 0){
				$result = array("data"=>array("status"=>1, "status_detail"=>"Successfully removed post.", "result"=>$del));
				return $result;
			}else{
				$result =  array("data"=>array("status"=>0, "status_detail"=>"Error removing post.", "result"=>""));
				return $result;
			}
		}else if($type === "readfull"){
			return $posts->read($post_id);
		}
		return array("data"=>array("status"=>0, "status_detail"=>"Error.", "result"=>""));
	}

    public function authenticate($usr, $pass){
        $user = new Application_Model_DbTable_Users();
        $result = $user->auth($usr, $pass);
        if($result['data']['status'] === 1){
            $user_details = new Zend_Session_Namespace('user_details');
            $user_details->userid = $result['data']['result']['userid'];
            $user_details->username = $result['data']['result']['username'];
            $user_details->useremail = $result['data']['result']['useremail'];
        }
        //var_dump($result['data']);
        return $result;
    }
    
    public function logout(){
        $user_details = new Zend_Session_Namespace('user_details');
        if($user_details->isLocked()){
            $user_details->unlock();
        }
        Zend_Session::namespaceUnset('user_details');
        return array("data"=>array("status"=>1, "status_detail"=>"Logged out.", "result"=>""));
    }
    
    function forgot_password(){
        
    }
    
    public function register($email, $username)
    {
        $user = new Application_Model_DbTable_Users();
        $verify_code = rand(10000,99999);
        $result = $user->create_user($username, $email, $verify_code);
        if($result === 0){
            return array("data"=>array("status"=>0, "status_detail"=>"This user already exists.", "result"=>""));
        }else{
            $local = str_replace("localhost", "localhost/newstand/public","http://".$_SERVER['HTTP_HOST'] );
            $link = $local . "/Index?allow=".$verify_code."&trace=".$result;
            $register_temp = file_get_contents($local . "/template.php");
            $new_mail = str_replace('[name]', $username, $register_temp);
            $new_mail = str_replace('[url]', $link, $new_mail);
            $mail = $this->plainSend("Welcome to Newstand", $new_mail, $email);
            return array("data"=>array("status"=>1, "status_detail"=>"Successfully Registered. Please, check your mailbox for next step.", "result"=>$mail));
        }
    }
    
    public function verify($id, $code, $password=null){
        extract(filter_input_array(INPUT_POST));
        $user = new Application_Model_DbTable_Users();
        $result = $user->verification($id, $code, $password);
        if($result === 1){
            //this guy is verified, created password and set verification code to null
            return array("data"=>array("status"=>1, "status_detail"=>"User verified and password changed.", "result"=>$id));
        }else{
            return array("data"=>array("status"=>0, "status_detail"=>"This is a wrong verification code, please click on the exact link provided.", "result"=>""));
        }
    }
    
    
    function upload_news(){
        //return $this->submit_post();
        //Make sure u call submit_post functionn here
        $ThumbSquareSize 		= 150; //Thumbnail will be 150X150
	$BigImageMaxSize 		= 250; //Image Maximum height or width
	$ThumbPrefix			= "thumb_"; //Normal thumb Prefix
	$DestinationDirectory	= 'img/'; //specify upload directory ends with / (slash)
	$Quality 				= 90; //jpeg quality
        if(null === filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')){
            //die("Not an ajax request");
            return array("data"=>array("status"=>0, "status_detail"=>"This request is not appropriate.", "result"=>""));
        }
	
	// check $_FILES['ImageFile'] not empty
	if(!isset($_FILES['ImageFile']) || !is_uploaded_file($_FILES['ImageFile']['tmp_name'])){
            //die('Something wrong with uploaded file, something missing!'); // output error when above checks fail.
            return array("data"=>array("status"=>0, "status_detail"=>"Image upload failed.", "result"=>""));
	}
	
	// Random number will be added after image name
	$RandomNumber 	= rand(0, 9999999999); 
	$ImageName 		= str_replace(' ','-',strtolower($_FILES['ImageFile']['name'])); //get image name
	$ImageSize 		= $_FILES['ImageFile']['size']; // get original image size
	$TempSrc	 	= $_FILES['ImageFile']['tmp_name']; // Temp name of image file stored in PHP tmp folder
	$ImageType	 	= $_FILES['ImageFile']['type']; //get file type, returns "image/png", image/jpeg, text/plain etc.

	//Let's check allowed $ImageType, we use PHP SWITCH statement here
	switch(strtolower($ImageType))
	{
            case 'image/png':
                //Create a new image from file 
                $CreatedImage =  imagepng($_FILES['ImageFile']['tmp_name']);
                break;
            case 'image/gif':
                $CreatedImage =  imagecreatefromgif($_FILES['ImageFile']['tmp_name']);
                break;			
            case 'image/jpeg':
            case 'image/pjpeg':
                $CreatedImage = imagecreatefromjpeg($_FILES['ImageFile']['tmp_name']);
                break;
            default:
                return array("data"=>array("status"=>0, "status_detail"=>"Format not supported.", "result"=>""));
                //die('Unsupported File!'); //output error and exit
	}
	
	//PHP getimagesize() function returns height/width from image file stored in PHP tmp folder.
	//Get first two values from image, width and height. 
	//list assign svalues to $CurWidth,$CurHeight
	list($CurWidth,$CurHeight)=getimagesize($TempSrc);
	
	//Get file extension from Image name, this will be added after random name
	$ImageExt = substr($ImageName, strrpos($ImageName, '.'));
  	$ImageExt = str_replace('.','',$ImageExt);
	
	//remove extension from filename
	$ImageName 		= preg_replace("/\\.[^.\\s]{3,4}$/", "", $ImageName); 
	
	//Construct a new name with random number and extension.
	//$NewImageName = $_POST['userid'] . $ImageName.'-'.$RandomNumber.'.'.$ImageExt;
	$NewImageName = $this->userid .'-'.$RandomNumber.".". $ImageExt;//userid
	
	//set the Destination Image
	$thumb_DestRandImageName = $DestinationDirectory.$ThumbPrefix.$NewImageName; //Thumbnail name with destination directory
	$DestRandImageName = $DestinationDirectory.$NewImageName; // Image with destination directory
	
	//Resize image to Specified Size by calling resizeImage function.
	if($this->resizeImage($CurWidth,$CurHeight,$BigImageMaxSize,$DestRandImageName,$CreatedImage,$Quality,$ImageType))
	{
            //Create a square Thumbnail right after, this time we are using cropImage() function
            if(!$this->cropImage($CurWidth,$CurHeight,$ThumbSquareSize,$thumb_DestRandImageName,$CreatedImage,$Quality,$ImageType))
            {
                    //echo 'Error Creating thumbnail';
                    return array("data"=>array("status"=>0, "status_detail"=>"Could not create thumbnail.", "result"=>""));
            }
            $img = $ThumbPrefix.$NewImageName;
	}else{
		//die('Resize Error'); //output error
            return array("data"=>array("status"=>0, "status_detail"=>"Error resizing image to acceptable dimension.", "result"=>""));
	}
        return $this->submit_post($img);
        //return array("data"=>array("status"=>1, "status_detail"=>"Upload was successful.", "result"=>""));
    }
    
    /**********Image resizing functions*******/
    // This function will proportionally resize image 
    function resizeImage($CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType)
    {
        //Check Image size is not 0
        if($CurWidth <= 0 || $CurHeight <= 0) 
        {
                return false;
        }

        //Construct a proportional size of new image
        $ImageScale      	= min($MaxSize/$CurWidth, $MaxSize/$CurHeight); 
        $NewWidth  		= ceil($ImageScale*$CurWidth);
        $NewHeight 		= ceil($ImageScale*$CurHeight);
        $NewCanves 		= imagecreatetruecolor($NewWidth, $NewHeight);

        // Resize Image
        if(imagecopyresampled($NewCanves, $SrcImage,0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight))
        {
                switch(strtolower($ImageType))
                {
                        case 'image/png':
                                //imagepng($NewCanves,$DestFolder);
                                break;
                        case 'image/gif':
                                //imagegif($NewCanves,$DestFolder);
                                break;			
                        case 'image/jpeg':
                        case 'image/pjpeg':
                                //imagejpeg($NewCanves,$DestFolder,$Quality);
                                break;
                        default:
                                return false;
                }
        //Destroy image, frees memory	
        if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
        return true;
        }

    }

    //This function corps image to create exact square images, no matter what its original size!
    function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
    {	 
        //Check Image size is not 0
        if($CurWidth <= 0 || $CurHeight <= 0) 
        {
                return false;
        }

        //abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
        if($CurWidth>$CurHeight)
        {
                $y_offset = 0;
                $x_offset = ($CurWidth - $CurHeight) / 2;
                $square_size 	= $CurWidth - ($x_offset * 2);
        }else{
                $x_offset = 0;
                $y_offset = ($CurHeight - $CurWidth) / 2;
                $square_size = $CurHeight - ($y_offset * 2);
        }

        $NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
        if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
        {
            switch(strtolower($ImageType))
            {
                case 'image/png':
                        imagepng($NewCanves,$DestFolder);
                        break;
                case 'image/gif':
                        imagegif($NewCanves,$DestFolder);
                        break;			
                case 'image/jpeg':
                case 'image/pjpeg':
                        imagejpeg($NewCanves,$DestFolder,$Quality);
                        break;
                default:
                        return false;
            }
        //Destroy image, frees memory	
        if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
        return true;

        }

    }
}













