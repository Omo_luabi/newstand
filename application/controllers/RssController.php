<?php

class RssController extends Zend_Controller_Action
{

    public function init()
    {
        //init for my API Class
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout('authlayout');
    }
    public function indexAction()
    {
        // action body
        $posts = new Application_Model_DbTable_Posts();
        $post_array = $posts->read_recent_posts();
        //var_dump($post_array->news);
        $count = count($post_array);
        $arr = array();
        if($count > 0){
            for($i = 0; $i < count($post_array); $i++){
                $arr[$i] = array('title'=>$post_array[$i]['newstitle'],
                    'description'=> (strlen($post_array[$i]['newscontent']) >100) ? substr($post_array[$i]['newscontent'], 0, 100) : $post_array[$i]['newscontent'],
                    'link'=>$post_array[$i]['newsid'],
                    'guid'=>$post_array[$i]['newsid']);
            }
        }else{
            
            $arr = array(
                    'title'=>'',
                    'description'=>'',
                    'link'=>'',
                    'guid'=>''
                );
        }
//        var_dump($arr);
        $feedData = array(
            'title'=>'Newstand',
            'description'=>'This is an rss feed for the newstand web application.',
            'link'=>'http://localhost/newstand',
            'charset'=>'utf8',
            'entries'=>$arr
        );
        $feed = Zend_Feed::importBuilder(new Zend_Feed_Builder($feedData),'rss');
        header('Content-type: text/xml');
        echo $feed->send();
    }


}

