<?php

class Application_Model_DbTable_Posts extends Zend_Db_Table_Abstract
{

    protected $_name = 'newstand_news';
    
    public function read_recent_posts(){
        $db     = $this;//return $row->toArray();
        $select_object = $db->select()->setIntegrityCheck(false); 
        $select_object->from('newstand_news')
                ->join('newstand_users', 'newstand_users.userid = newstand_news.userid', array('username','useremail', 'userid'))
                ->order("newstand_news.newsid desc")
                ->limit(10);
        
        $post_read_array = $db->fetchAll($select_object);
        return $post_read_array->toArray();
    }
	
	public function read($post_id){
		$post = $this->fetchRow("newsid = $post_id");
		if (!$post) {
            $result =  array("data"=>array("status"=>0, "status_detail"=>"Couldn't read this post.", "result"=>""));
        }else{
			$rowArray = $post->toArray();
			$result =  array("data"=>array("status"=>1, "status_detail"=>"Success.", "title"=>$rowArray['newstitle'], "result"=>$rowArray['newscontent'], "image"=>$rowArray['newsimage'], "timestamp"=>$rowArray['newstimestamp']));
		}
		return $result;
	}
    
    public function create_post($array){
        $id = $this->insert($array);
        return $id;
    }
    
    public function delete_post($id, $user)
    {
        return $del = $this->delete('newsid =' . (int)$id." AND userid = $user");
    }

}

