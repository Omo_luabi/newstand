<?php

class Application_Model_DbTable_Users extends Zend_Db_Table_Abstract
{

    protected $_name = 'newstand_users';
    
    public function create_user($username, $useremail, $code){
        $user_ = $this->fetchAll($where="useremail='$useremail' OR username='$username'");
        if(count($user_) > 0){
            return 0;
            //return array("data"=>array("status"=>"0", "status_detail"=>"This user already exists.", "result"=>""));
        }else{
            //$dt = date("Y-m-d H:i:s");
            $data = array(
                "username"=>$username, 
                "useremail"=>$useremail, 
                "userverify"=>$code);
            $id = $this->insert($data);
            return $id;
        }
    }
    
    public function verification($id, $code, $password){
        $select_object = $this->select()->setIntegrityCheck(false); 
        $select_object->from('newstand_users');
        
        $row = $this->fetchRow("userid = $id AND userverify = $code");
        if (!$row) {
            return 0;
            //throw new Exception("Could not find user");
        }else{
            $rowArray = $row->toArray();
            $user_details = new Zend_Session_Namespace('user_details');
            $user_details->userid = $id;
            $user_details->username = $rowArray['username'];
            $user_details->useremail = $rowArray['useremail'];
            $data = array('userpassword' => $password,'userverify' => null,'useractive' => 1,);
            $update = $this->update($data, 'userid = '. (int)$id . " AND userverify = '$code'");
            if($update > 0){
                return 1;
            }
        }
        return 0;
//        return $row->toArray();
    }
    
    public function auth($username, $password){
        $select_object = $this->select()->setIntegrityCheck(false); 
        $select_object->from('newstand_users');
        
        $auth_array = $this->fetchRow("username = '$username'");// AND userpassword = $password
        if(!$auth_array){
            return array("data"=>array("status"=>0, "status_detail"=>"Incorrect Username.", "result"=>""));
        }
        $rowArray = $auth_array->toArray();
        
        if(count($rowArray) > 0){
            if($rowArray['userpassword'] == $password){
                return array("data"=>array("status"=>1, "status_detail"=>"Login Succsessful.", "result"=>$rowArray));
            }else{
                return array("data"=>array("status"=>0, "status_detail"=>"Incorrect Password.", "result"=>""));
            }
        }else{
            return array("data"=>array("status"=>0, "status_detail"=>"Incorrect Username.", "result"=>""));
        }
    }
    
    public function delete_post($id)
    {
        $this->delete('userid =' . (int)$id);
    }

}

