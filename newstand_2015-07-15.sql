# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.24)
# Database: newstand
# Generation Time: 2015-07-15 06:53:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table newstand_news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `newstand_news`;

CREATE TABLE `newstand_news` (
  `newsid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `newstitle` varchar(50) NOT NULL DEFAULT '',
  `newscontent` text NOT NULL,
  `newsimage` text NOT NULL,
  `newstimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `commentcount` int(11) NOT NULL DEFAULT '0',
  `readcount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsid`),
  KEY `userid` (`userid`),
  CONSTRAINT `newstand_news_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `newstand_users` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table newstand_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `newstand_users`;

CREATE TABLE `newstand_users` (
  `userid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL DEFAULT '',
  `useremail` text NOT NULL,
  `userpassword` text,
  `userlogtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `useractive` int(2) NOT NULL DEFAULT '0',
  `userverify` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
